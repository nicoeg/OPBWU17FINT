<?php

require_once 'Controller.php';

class FirstController extends Controller {

	public function index() {
		$this->loadModel('User');

		$users = $this->User->all();

		return $this->view('master', ['users' => $users]);
	}
}