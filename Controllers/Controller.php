<?php

class Controller {

	private $models = [];

	public function loadModel($model) {
		require_once 'Models/' . $model . '.php';

		$this->models[$model] = new $model;
	}

	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}

		if (key_exists($property, $this->models)) {
			return $this->models[$property];
		}

		throw new Exception("Property does not exist");
		
	}

	public function view($view, $data) {
		return include('Views/' . $view . '.php');
	}
}